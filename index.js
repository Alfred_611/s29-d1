/// require express module and save it in a document
const express = require("express");

// Create an application using express
const app = express();

// App server will listen to port 3000 // ito lang siguro yung nagbabago, yung port
const port = 3000;

// Setup for allowing the server to handle data from requests
// Allows the app to read json data
app.use(express.json());

// Allows the app to read data from forms
app.use(express.urlencoded({extended:true}));

// GET route 

app.get("/", (req, res) =>{
	res.send("Hello World");
})

app.get("/hello", (req, res) =>{
	res.send("Hello from the /hello endpoint!");
})

// POST route

app.post("/hello", (req, res) =>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
})

// Mock database for POST route
let users = [];

// Will create a signup POST
app.post("/signup", (req, res) => {
	console.log(req.body);

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`);
	}
	else{
		res.send("Please input BOTH username and password");
	}
})

// PUT request for changing the password
app.put("/change-password", (req, res) => {
	//Creates a variable to store the message to be sent back to the client/Postman
	let message;

	// Creates a for loop that will loop through the elements of the "users" array - ito ang titingin ng bawat element we have so far sa ating array
	for(let i = 0; i < users.length; i++){

		if(req.body.username === users[i].username){

			users[i].password = req.body.password;

			message = `User ${req.bodyusername}'s password has been updated.`;
		break;
		}
		else{
			message = "User does not exist"
		}
	}

	res.send(message);
})


// ACTIVITY
//1. Create a GET route that will access the "/home" route that will print out a simple message
app.get("/home", (req, res) =>{
	res.send("Welcome to the home page");
})


//3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database
app.get("/users", (req, res) =>{
	res.send(users);
})


//5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database
app.delete("/delete-user", (req, res) =>{
	//Creates a variable to store the message to be sent back to the client/Postman
	let message;

	// Creates a for loop that will loop through the elements of the "users" array - ito ang titingin ng bawat element we have so far sa ating array
	for(let i = 0; i < users.length; i++){

		if(req.body.username === users[i].username){

			users[i].username = users.unshift(`${users[i].username}`);

			message = `User ${req.body.username} has been deleted.`;
		break;
		}
		else{
			message = "User does not exist"
		}
	}

	res.send(message);
})

// Tells the server to listen to the port - NASA LAST PART DAPAT ITO PALAGI
app.listen(port, () => console.log(`Server is running at port ${port}`));